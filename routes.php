<?php

return [
    [
        // Index
        'uri' => '/',
        'method' => 'GET',
        'controller' => 'App\Controllers\IndexController',
        'action' => 'show',
    ],

    [
        // Login page
        'uri' => '/auth/login',
        'method' => 'GET',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'showLogin',
    ],

    [
        // Login action
        'uri' => '/auth/login',
        'method' => 'POST',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'login',
    ],

    [
        // Logout action
        'uri' => '/auth/logout',
        'method' => 'GET',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'logout',
    ],

    [
        // Register page
        'uri' => '/auth/register',
        'method' => 'GET',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'showRegister',
    ],

    [
        // Register action
        'uri' => '/auth/register',
        'method' => 'POST',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'register',
    ],

    [
        // Confirmation page
        'uri' => '/auth/confirm',
        'method' => 'GET',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'showConfirm',
    ],

    [
        // Confirmation action
        'uri' => '/auth/confirm',
        'method' => 'POST',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'confirm',
    ],

    [
        // Password reset page/action
        'uri' => '/auth/reset',
        'method' => 'GET',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'showResetLink',
    ],

    [
        // Password reset page
        'uri' => '/auth/reset/{token}',
        'method' => 'GET',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'showReset',
    ],

    [
        // Password reset action
        'uri' => '/auth/reset/{token}',
        'method' => 'POST',
        'controller' => 'App\Controllers\AuthController',
        'action' => 'reset',
    ],

    [
        // User profile
        'uri' => '/user',
        'method' => 'GET',
        'controller' => 'App\Controllers\UserController',
        'action' => 'show',
    ],

    [
        // Notifications toggle
        'uri' => '/user/edit/notifications',
        'method' => 'GET',
        'controller' => 'App\Controllers\UserController',
        'action' => 'notifications',
    ],

    [
        // User field edition
        'uri' => '/user/edit/{field}',
        'method' => 'POST',
        'controller' => 'App\Controllers\UserController',
        'action' => 'edit',
    ],

    [
        // Upload page
        'uri' => '/upload',
        'method' => 'GET',
        'controller' => 'App\Controllers\UploadController',
        'action' => 'show',
    ],

    [
        // Upload action
        'uri' => '/upload',
        'method' => 'POST',
        'controller' => 'App\Controllers\UploadController',
        'action' => 'upload'
    ],

    [
        // Single picture page
        'uri' => '/picture/{id}',
        'method' => 'GET',
        'controller' => 'App\Controllers\ImageController',
        'action' => 'show',
    ],

    [
        // Picture removal
        'uri' => '/picture/delete/{id}',
        'method' => 'POST',
        'controller' => 'App\Controllers\ImageController',
        'action' => 'delete',
    ],

    [
        // Picture field edition
        'uri' => '/picture/edit/{id}',
        'method' => 'POST',
        'controller' => 'App\Controllers\ImageController',
        'action' => 'edit',
    ],

    [
        // Picture like action
        'uri' => '/picture/like/{id}',
        'method' => 'GET',
        'controller' => 'App\Controllers\ImageController',
        'action' => 'like',
    ],

    [
        // Comment storing
        'uri' => '/comment/store/{image_id}',
        'method' => 'POST',
        'controller' => 'App\Controllers\CommentController',
        'action' => 'store',
    ],

    [
        // Comment removal
        'uri' => '/comment/delete/{id}',
        'method' => 'POST',
        'controller' => 'App\Controllers\CommentController',
        'action' => 'delete'
    ],

    [
        // Comment editing
        'uri' => '/comment/edit/{id}',
        'method' => 'POST',
        'controller' => 'App\Controllers\CommentController',
        'action' => 'edit'
    ],
];

