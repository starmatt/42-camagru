<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Language" content="en-us">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <style>
        * {
            box-sizing: border-box;
        }

        .email {
            width: 100%;
            max-width: 480px;
            background-color: #000;
            color: #95a3b3;
            margin: auto;
            padding: 25px;
            border-radius: 4px;
        }
    </style>
</head>
<body>
    <div class="email">
        <img style="display:block;width:215px;height:65px;margin:auto" src="https://i.imgur.com/Y5jdMS1.png" alt="Logo">
        <br>

        <p>Welcome aboard <?= __($user->username) ?>!</p>

        <p>Copy/paste this token on the confirmation page to activate your account:</p>
        <p style="text-align:center;color:#22aed1;font-size:24px"><b><?= $user->activation_token ?></b></p>
    </div>
</body>
</html>
