<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Language" content="en-us">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

    <style>
        * {
            box-sizing: border-box;
        }

        .email {
            width: 100%;
            max-width: 480px;
            background-color: #000;
            color: #95a3b3;
            margin: auto;
            padding: 25px;
            border-radius: 4px;
        }
    </style>
</head>
<body>
    <div class="email">
        <img style="display:block;width:215px;height:65px;margin:auto" src="https://i.imgur.com/Y5jdMS1.png" alt="Logo">
        <br>

        <p>Hello, it seems you have requested a password reset.</p>

        <p>Follow this location to create a new password for your account: <pre style="color:#22aed1">/auth/reset/<?= $token ?></pre></p>
    </div>
</body>
</html>
