<div class="font-alien title">Gallery</div>

<?php if ($images): ?>
    <?php foreach ($images as $image): ?>
        <div class="std-box mb-25">
            <div class="d-flex m-auto">
                <a href="/picture/<?= $image->id ?>" class="overlay">
                    <div class="thumbnail"><img class="gallery-image" src="<?= __($image->location()) ?>" alt="An Image!"></div>
                </a>
                <div class="flex-1 m-auto">
                    <div class="mb-10"><span class="lnr lnr-user"></span> <?= __($image->user()->username) ?></div>
                    <hr>
                    <div class="mb-10"><span class="lnr lnr-heart"></span> <?= count($image->likes()) ?> like(s)</div>
                    <div><span class="lnr lnr-bubble"></span> <?= count($image->comments()) ?> comment(s)</div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
<?php else: ?>
    <div class="std-box mb-25 text-center">
        No images have been created yet.<br><a href="/upload">Make your own now!</a>
    </div>
<?php endif ?>

<?php if ($images && $max_page >= 2): ?>
    <ul class="pagination">
        <?php if ($current - 1 < 1): ?>
            <li><</li>
        <?php else: ?>
            <a href="<?= '/?page=' . ($current - 1) ?>"><li><</li></a>
        <?php endif ?>
    
        <?php for ($page = 1; $page <= $max_page; $page++): ?>
            <a class="<?= $page == $current ? 'current' : '' ?>" href="<?= "/?page=$page" ?>"><li><?= $page ?></li></a>
        <?php endfor; ?>
    
        <?php if ($current + 1 > $max_page): ?>
            <li>></li>
        <?php else: ?>
            <a href="<?= '/?page=' . ($current + 1) ?>"><li>></li></a>
        <?php endif ?>
    </ul>
<?php endif ?>