<div class="font-alien title">Profile and settings</div>

<?php if ($errors): ?>
    <ul class="std-box sm error">
        <?php foreach ($errors as $error): ?>
            <li><?= __($error) ?></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>

<div class="std-box mb-25">
    <div class="font-alien mb-5">Username</div>
    <span class="relative wrap"><?= __($user->username) ?> <a href="#" data-form="edit-username" class="lnr lnr-pencil edit-button"></a></span>

    <form id="edit-username" method="POST" action='/user/edit/username' class="auth hidden mt-10">
        <input type="text" name="username" placeholder="Change your handle" class="mb-10">
        <button class="btn">Edit</button>
    </form>
</div>

<div class="std-box mb-25">
    <div class="font-alien mb-5">E-mail</div>
    <span class="relative wrap"><?= __($user->email) ?> <a href="#" data-form="edit-email" class="lnr lnr-pencil edit-button"></a></span>

    <form id="edit-email" method="POST" action='/user/edit/email' class="auth hidden mt-10">
        <input type="email" name="email" placeholder="Change your e-mail address" class="mb-10">
        <button class="btn">Edit</button>
    </form>
</div>

<div class="std-box mb-25">
    <div for="email" class="font-alien mb-5">Password</div>
    <a href="/auth/reset?email=<?= urlencode($user->email) ?>">Request a password reset link</a>
</div>

<div class="std-box mb-25">
    <div class="font-alien mb-5">Notifications</div>
    <div>When someone comments my pictures: <a href="/user/edit/notifications"><?= $user->notifications ? 'ON' : 'OFF' ?></a></div>
</div>

<div class="title font-alien mt-25">Favorites</div>

<div id="favorites" class="">
    <div class="d-flex">
        <?php if (!empty($images)): ?>
            <?php foreach ($images as $image): ?>
                <a class="overlay" href="/picture/<?= $image->id ?>">
                    <img class="" src="<?= __($image->location()) ?>">
                </a>
            <?php endforeach ?>
        <?php else: ?>
            <div class="text-center">Like pictures to add them to your favorites.</div>
        <?php endif ?>
    </div>
</div>