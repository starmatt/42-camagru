<div id="upload-page" class="d-flex mobile-column relative">
    <div id="upload-column">
        <div class="font-alien title">Filters</div>

        <div class="std-box mb-25">
            <div class="filters-select d-flex">
                <div class="d-flex flex-1 space-around">
                    <label class="d-flex filter-wrapper" for="filter-astronaut">
                        <input class="radio-filter" type="radio" id="filter-astronaut" name="filter" value="astronaut">
                        <img src="/public/images/filters/astronaut.png">
                    </label>
    
                    <label class="d-flex filter-wrapper" for="filter-xenomorph">
                        <input class="radio-filter" type="radio" id="filter-xenomorph" name="filter" value="xenomorph">
                        <img src="/public/images/filters/xenomorph.png">
                    </label>
                </div>

                <div class="d-flex flex-1 space-around">
                    <label class="d-flex filter-wrapper" for="cockpit">
                        <input class="radio-filter" type="radio" id="cockpit" name="filter" value="cockpit">
                        <img src="/public/images/filters/cockpit.png">
                    </label>

                    <label class="d-flex filter-wrapper" for="cockpit_2">
                        <input class="radio-filter" type="radio" id="cockpit_2" name="filter" value="cockpit_2">
                        <img src="/public/images/filters/cockpit_2.png">
                    </label>
                </div>
            </div>
        </div>

        <div class="font-alien title">
            <span id="upload-click" class="clickable gray selected">Upload</span> | <span id="webcam-click" class="clickable gray">Webcam</span>
        </div>

        <div id="upload-wrapper">
            <div id="error-div" class="std-box sm error <?= isset($errors['upload']) ? '' : 'hidden' ?>"><?= __($errors['upload']) ?></div>

            <div class="std-box">
                <div class="mb-25 yellow"><span class="lnr lnr-warning"></span> Max filesize is <?= __(ini_get('upload_max_filesize')) ?>.</div>

                <div id="upload-preview" class="mb-25">
                    <div class="filter-overlay">
                        <img class="filter-img hidden" src="#">
                        <img id="preview" class="hidden" src="#">
                    </div>
                </div>

                <div id="upload-group" class="d-flex">
                    <div class="upload-wrapper relative">
                        <label id="label-upload" for="upload-input">Select an image</label> <span id="clear-upload" class="hidden red lnr lnr-cross"></span>
                    </div>

                    <button class="btn upload-btn" disabled>Upload</button>
                </div>
            </div>
        </div>

        <div id="webcam-wrapper" class="hidden">
            <div id="webcam-status" class="std-box sm error hidden"></div>

            <div class="std-box">
                <div id="stream-preview">
                    <div class="filter-overlay">
                        <img class="filter-img hidden" src="#" alt="">
                        <video autoplay="true" id="stream"></video>
                    </div>
                    <canvas id="canvas" width="640" height="480" class="hidden"></canvas>
                </div>

                <button id="webcam-btn" class="btn upload-btn" disabled>Shoot</button>
            </div>
        </div>
    </div>

    <div id="history-column">
        <div class="font-alien title">History</div>

        <div class="history d-flex">
            <?php if (!empty($images)): ?>
                <?php foreach ($images as $image): ?>
                    <a class="overlay" href="/picture/<?= $image->id ?>">
                        <img class="" src="<?= __($image->location()) ?>">
                    </a>
                <?php endforeach ?>
            <?php else: ?>
                <div class="text-center">You haven't taken any pictures yet!</div>
            <?php endif ?>
        </div>
    </div>

    <form id="upload-form" class="hidden" method="POST" action="/upload" enctype="multipart/form-data">
        <input type="hidden" name="filter">
        <input type="hidden" name="selected">
        <input id="upload-input" name="upload" type="file" accept="image/*">
        <input id="webcam-input" type="hidden" name="webcam">
    </form>
</div>
