<div class="title font-alien">REGISTER</div>

<?php if ($errors): ?>
    <ul class="std-box sm error">
            <?php foreach ($errors as $error): ?>
                <li><?= __($error) ?></li>
            <?php endforeach ?>
        </ul>
<?php endif ?>

<div class="std-box">
    <form class="auth m-auto flex-1" method="post" action="/auth/register">
        <div class="input-group">
            <label for="username">Username</label>
            <input id="username" type="text" name="username" value="<?= __(flash('username')) ?>">
        </div>

        <div class="input-group">
            <label for="email">E-mail address</label>
            <input id="email" type="text" name="email" value="<?= __(flash('email')) ?>">
        </div>

        <div class="input-group">
            <label for="password">Password</label>
            <input id="password" type="password" name="password">
        </div>

        <div class="input-group">
            <label for="password_confirmation">Confirmation</label>
            <input id="password_confirmation" type="password" name="password_confirmation">
        </div>

        <button class="btn d-block m-auto">Submit</button>
    </form>
</div>
