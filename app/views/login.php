<div class="title font-alien">Log-in</div>

<?php if ($errors): ?>
    <ul class="std-box sm error mb-5">
        <?php foreach ($errors as $error): ?>
            <li><?= __($error) ?></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>

<div class="std-box">
    <form class="auth m-auto flex-1" method="post" action="/auth/login/">
        <div class="input-group">
            <label for="email">E-mail address</label> 
            <input type="email" name="email" value="<?= __(flash('email')) ?>">
        </div>

        <div class="input-group">
            <label for="password">Password</label>
            <input type="password" name="password">
        </div>

        <button class="btn d-block m-auto">Submit</button>

        <div class="text-center mt-25">
            <a href="/auth/register">Don't have an account? Click here to create one.</a>

            <hr>

            <a href="/auth/reset">Request a password reset link.</a>
        </div>
    </form>
</div>