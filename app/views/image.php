<?php if ($errors && isset($errors['title'])): ?>
    <ul class="std-box sm error mb-25">
        <li><?= __($errors['title']) ?></li>
    </ul>
<?php endif ?>

<div class="font-alien title relative">
    <span class="relative wrap"><?= $image->title ? __($image->title) : "Image #$image->id" ?><?php if (auth() && auth_user()->id === $user->id): ?> <a href="#" data-form="image-title-edit" class="lnr lnr-pencil edit-button"></a><?php endif ?></span>

    <?php if (auth() && auth_user()->id === $user->id): ?>
        <form id="image-title-edit" class="hidden edit-form" method="POST" action="/picture/edit/<?= $image->id ?>">
            <input type="text" placeholder="Edit your image's title" name="title" value="<?= __($image->title) ?>"><button class="btn">Apply</button>
        </form>
    <?php endif ?>
</div>

<?php if ($errors && isset($errors['description'])): ?>
    <ul class="std-box sm error">
        <li><?= __($errors['description']) ?></li>
    </ul>
<?php endif ?>

<div class="image-full relative">
    <img src="<?= __($image->location()) ?>">
    <?php if (auth() && auth_user()->id === $user->id): ?>
        <form id="image-delete" method="post" action="/picture/delete/<?= $image->id ?>">
            <button><span class="lnr lnr-trash"></span></button>
        </form>
    <?php endif ?>
</div>

<div class="std-box mb-25">
    <div class="mb-25 d-flex">
        <div class="mb-10 flex-1">Credits: <span class="yellow"><?= __($user->username) ?></span> <span class="text-sm">(<?= date('d/m/Y \a\t H\hi', strtotime($image->created_at)) ?>)</span></div>
        <div class="m-auto">
            <?php if (guest()): ?>
                <span class="btn inline" disabled><span class="lnr lnr-heart"></span> <?= count($likes) ?> like(s)</span>
            <?php else: ?>
                <a class="btn inline" href="/picture/like/<?= $image->id ?>"><span class="lnr lnr-heart"></span> <?= count($likes) ?> like(s)</a>
            <?php endif ?>
        </div>
    </div>

    <div class="relative">
        <span class="relative wrap">
            <?= $image->description ? __($image->description) : 'No description'; ?>
            <?php if (auth() && auth_user()->id === $user->id): ?>
                <a href="#" data-form="image-description-edit" class="lnr lnr-pencil edit-button"></a>
            <?php endif ?>
        </span>

        <?php if (auth() && auth_user()->id === $user->id): ?>
            <form id="image-description-edit" class="hidden edit-form" method="POST" action="/picture/edit/<?= $image->id ?>">
                <textarea name="description" placeholder="Edit your image's description"><?= __($image->description) ?></textarea>
                <button class="btn">Apply</button>
            </form>
        <?php endif ?>
    </div>
</div>

<div class="font-alien title">Comments <?= count($comments) ? '(' . count($comments) . ')' :  '' ?></div>

<?php if ($errors && isset($errors['content'])): ?>
    <ul class="std-box sm error">
        <li><?= __($errors['content']) ?></li>
    </ul>
<?php endif ?>

<div class="mb-25">
    <?php if (guest()): ?>
        <div class="text-center"><a href="/auth/login">Log-in</a> to add a comment.</div>
    <?php else: ?>
        <form id="comment-form" method="POST" action="/comment/store/<?= $image->id ?>">
            <input class="mb-10 flex-1" type="text" name="content" placeholder="Add a thought...">
            <button class="btn">Comment</button>
        </form>
    <?php endif ?>
</div>

<?php foreach ($comments as $comment): ?>
    <div class="text-sm mb-5">
        By <span class="yellow"><?= __($comment->user()->username) ?></span> (<?= date('d/m/Y \a\t H\hi', strtotime($comment->created_at)) ?>)
        <?php if (auth() && auth_user()->id === $comment->user()->id): ?>
            <span class="i-pad"><a href="#" data-comment="<?= $comment->id ?>" class="comment-edit lnr lnr-pencil"></a></span>
            <form class="i-pad comment-delete" method="POST" action="/comment/delete/<?= $comment->id ?>">
                <button class="bare i-pad red lnr lnr-trash"></button>
            </form>
        <?php endif ?>
    </div>

    <?php if ($errors && isset($errors["comment-$comment->id"])): ?>
        <ul class="std-box sm error">
            <li><?= __($errors["comment-$comment->id"]) ?></li>
        </ul>
    <?php endif ?>

    <div class="std-box sm mb-25">
        <div id="comment-<?= $comment->id ?>" class="comment">
            <?= __($comment->content) ?>
        </div>
    </div>
<?php endforeach ?>
