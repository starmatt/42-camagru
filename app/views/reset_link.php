<div class="title font-alien">Password reset</div>

<?php if ($errors): ?>
    <ul class="std-box sm error">
        <?php foreach ($errors as $error): ?>
            <li><?= __($error) ?></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>

<div class="std-box">
    <form class="auth m-auto flex-1" method="get" action="/auth/reset">
        <div class="mb-25 yellow"><span class="lnr lnr-warning"></span> Please provide the e-mail address of your account to receive a password reset link.</div>

        <div class="input-group">
            <label for="email">E-mail address</label>
            <input id="email" type="email" name="email">
        </div>

        <button class="btn d-block m-auto">Submit</button>
    </form>
</div>
