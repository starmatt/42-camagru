<div class="title font-alien">E-mail confirmation</div>

<?php if ($errors): ?>
    <ul class="std-box sm error">
        <?php foreach ($errors as $error): ?>
            <li><?= __($error) ?></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>

<div class="std-box">
    <form class="auth m-auto flex-1" method="post" action="/auth/confirm">
        <div class="mb-25 yellow"><span class="lnr lnr-warning"></span> Please provide the activation token that has been sent to your e-mail address.</div>

        <div class="input-group">
            <label for="token">Activation token</label>
            <input id="token" type="text" name="token">
        </div>

        <input type="hidden" name="email" value="<?= __($email) ?>">

        <button class="btn d-block m-auto">Submit</button>
    </form>
</div>
