<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Camagru</title>
    <link rel="icon" type="image/png" href="/public/images/icon.png">
    <link rel="stylesheet" href="/public/css/normalize.css">
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="stylesheet" href="/public/css/linearicons/style.css">
</head>

<body>
    <header class="relative">
        <div class="d-flex flex-1">
            <div class="brand-logo font-alien"><a href="/" class="brand">CAMAGRU</a></div>

            <div class="desktop">
                <ul id="nav">
                    <?php if (auth()): ?>
                        <li class="nav-item"><a href="/upload"><span class="lnr lnr-camera"></span></a></li>
                        <li class="nav-item"><a href="/user"><span class="lnr lnr-cog"></span></a></li>
                        <li class="nav-item"><a href="/auth/logout"><span class="lnr lnr-exit"></span></a></li>
                    <?php else: ?>
                        <li class="nav-item"><a href="/auth/login"><span class="lnr lnr-enter"></span></a></li>
                    <?php endif ?>
                </ul>
            </div>

            <div class="mobile">
                <div class="menu-btn"><span class="lnr lnr-menu"></span></div>

                <div id="mobile-nav" class="">
                    <hr>
                    <ul>
                        <?php if (auth()): ?>
                            <li class="nav-item"><a href="/upload"><span class="lnr lnr-camera"></span>Upload</a></li>
                            <li class="nav-item"><a href="/user"><span class="lnr lnr-cog"></span>Settings</a></li>
                            <li class="nav-item"><a href="/auth/logout"><span class="lnr lnr-exit"></span>Logout</a></li>
                        <?php else: ?>
                            <li class="nav-item"><a href="/auth/login"><span class="lnr lnr-enter"></span>Login</a></li>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="main container">
        <?php if ($success): ?>
            <div class="mt-25 mb-25">
                <div class="std-box sm success"><?= __($success) ?></div>
            </div>
        <?php endif ?>

        <?php if (isset($view)) require_once $view; ?>
    </div>

    <footer>
        <div>Camagru by <a target="_blank" href="https://profile.intra.42.fr/users/mborde">mborde</a> • 2018</div>
    </footer>

    <script type="text/javascript" src="/public/js/app.js"></script>
</body>
</html>
