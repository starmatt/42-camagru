<?php

namespace App\Models;

use Internals\Model;

class Like extends Model
{
    protected $table = 'likes';

    protected $attributes = [
        'user_id',
        'image_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id');
    }
}
