<?php

namespace App\Models;

use Internals\Model;

class Image extends model
{
    protected $table = 'images';

    protected $attributes = [
        'user_id',
        'title',
        'path',
        'description'
    ];

    private $storage = '/public/images/uploads';

    public function location()
    {
        if (file_exists(DIR_PUBLIC . '/images/uploads/' . $this->path)) {
            return "$this->storage/$this->path";
        }

        return '/public/images/black_hole.png';
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'image_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like', 'image_id');
    }
}
