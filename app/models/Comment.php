<?php

namespace App\Models;

use Internals\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $attributes = [
        'user_id',
        'image_id',
        'comment_id',
        'content',
    ];

    public function is_reply()
    {
        return $this->comment_id ? true : false;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Comment', 'comment_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Models\Comment', 'comment_id');
    }
}
