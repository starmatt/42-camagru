<?php

namespace App\Models;

use Internals\Model;

class PasswordReset extends Model
{
    protected $table = 'password_resets';

    protected $attributes = [
        'user_id',
        'token',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}