<?php

namespace App\Models;

use Internals\Auth\Authenticable;

class User extends Authenticable
{
    protected $table = 'users';

    protected $attributes = [
        'email',
        'username',
        'password',
        'notifications',
        'activation_token',
    ];

    public function activationLink()
    {
        return $_SERVER['HTTP_REFERER'] . "confirm?email=$this->email&token=$this->activation_token";
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'user_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like', 'user_id');
    }

    public function passwordResetLink()
    {
        return $this->hasOne('App\Models\PasswordReset', 'user_id');
    }

    public function likedImages()
    {
        $likes = Like::find([
            'user_id:is' => $this->id,
        ]);

        $liked_images = [];

        if (is_array($likes)) {
            foreach ($likes as $like) {
                $liked_images[] = $like->image();
            }
        } else if ($likes) {
            $liked_images[] = $likes->image();
        }

        return $liked_images;
    }
}
