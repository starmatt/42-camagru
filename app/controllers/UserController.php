<?php

namespace App\Controllers;

use Internals\Controller;
use Internals\Request;
use Internals\Validator;
use App\Models\User;

class UserController extends Controller
{
	public function __construct()
	{
		if (guest()) {
			return $this->redirect('/auth/login');
		}
	}

	public function show()
	{
		$user = User::current();

		return $this->view('user', [
            'user' => $user,
            'images' => $user->likedImages(),
        ]);
	}

    public function notifications()
    {
        $user = User::current();

        $user->notifications = (int) !$user->notifications;
        $user->save();

        return $this->with('success', 'Settings updated.')->redirect(back());
    }

    public function edit(Request $request, $field)
    {
        if (!in_array($field, ['email','username'])) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        $user = User::current();

        if (!$request->input($field) || $request->input($field) === $user->{$field}) {
            return $this->redirect(back());
        }

        $v = new Validator($request->inputs(), [
            'email' => ['optional', 'max:80', 'email', 'unique:users'],
            'username' => ['optional', 'max:80', 'unique:users'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->redirect(back());
        }

        $user->{$field} = $request->input($field); 
        $user->save();

        return $this->with('success', 'Settings updated.')->redirect(back());
    }
}
