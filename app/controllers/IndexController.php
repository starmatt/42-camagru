<?php

namespace App\Controllers;

use Internals\Request;
use Internals\Controller;
use App\Models\User;
use App\Models\Image;

class IndexController extends Controller
{
    public function show(Request $request)
    {
        $page = $request->input('page') !== null ? $request->input('page') : 1;
        $limit = 5;
        $max_page = ceil(Image::count() / $limit);

        if (!is_numeric($page) || $page < 1 || ($max_page > 0 && $page > $max_page)) {
            return $this->redirect(back());
        }

        $images = Image::find('*', [], [
            'ORDER BY' => 'created_at DESC',
            'LIMIT' => $limit,
            'OFFSET' => $limit * ($page - 1),
        ]);

        return $this->view('welcome', [
            'images' => $images,
            'max_page' => $max_page,
            'current' => $page,
        ]);
    }
}
