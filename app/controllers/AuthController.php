<?php

namespace App\Controllers;

use Internals\Request;
use Internals\Controller;
use Internals\Validator;
use Internals\Mail;
use App\Models\User;
use App\Models\PasswordReset;

class AuthController extends Controller
{
    public function showLogin()
    {
        if (auth()) {
            return $this->redirect(back());
        }

        return $this->view('login');
    }

    public function showRegister()
    {
        if (auth()) {
            return $this->redirect(back());
        }

        return $this->view('register');
    }

    public function showConfirm(Request $request)
    {
        if (auth()) {
            return $this->redirect(back());
        }

        $email = urldecode($request->input('email'));

        if (!$email) {
            return $this->redirect(back());
        }

        return $this->view('confirm', [
            'email' => $email,
        ]);
    }

    public function showResetLink(Request $request)
    {
        if ($request->has('email')) {
            return $this->sendResetLink($request);
        }

        return $this->view('reset_link');
    }

    public function showReset(Request $request, $token)
    {
        $reset = PasswordReset::find(['token:is' => $token]);

        if ($reset === null || empty($reset)) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $user = $reset[0]->user();

        if (!$user) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        return $this->view('reset_password', [
            'token' => $token,
        ]);
    }

    public function logout()
    {
        if (guest()) {
            return $this->redirect(back());
        }

        User::logout();

        return $this->with('success', 'Log-out successful')->redirect('/');
    }

    public function register(Request $request)
    {
        $v = new Validator($request->inputs(), [
            'email' => ['required', 'email', 'unique:users', 'max:80'],
            'username' => ['required', 'unique:users', 'max:80'],
            'password' => ['required', 'min:8', 'complex'],
            'password_confirmation' => ['required', 'match:password']
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->with('email', $request->input('email'))
                ->with('username', $request->input('username'))
                ->redirect(back());
        }

        $user = new User;
        $user->email = $request->input('email');
        $user->password = User::hash($request->input('password'));
        $user->username = $request->input('username');
        $user->activation_token = str_random(8);
        $user->notifications = true;
        $user->save();

        Mail::send($user->email, 'Camagru * E-mail confirmation', 'email', [
            'user' => $user,
            'token' => $user->activation_token,
        ]);

        return $this->redirect('/auth/confirm?email=' . urlencode($user->email));
    }

    public function login(Request $request)
    {
        $v = new Validator($request->inputs(), [
            'email' => ['required', 'email', 'exists:users,email'],
            'password' => ['required'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->with('email', $request->input('email'))
                ->redirect(back());
        }

        $user = User::find([
            'email:is' => $request->input('email'),
        ])[0];

        if (!$user->is_active()) {
            return $this->with('errors', [
                'confirm' => 'Your account has not been confirmed yet, check your e-mails for your activation token.'
            ])->redirect('/auth/confirm?email=' . urlencode($user->email));
        }

        if (!$user->authenticate($request->input('password'))) {
            return $this->with('errors', ['password' => 'This password is incorrect.'])
                ->with('email', $request->input('email'))
                ->redirect(back());
        }

        return $this->with('success', "Welcome aboard, $user->username")
            ->redirect('/');
    }

    public function confirm(Request $request)
    {
        $v = new Validator($request->inputs(), [
            'email' => ['required', 'email', 'exists:users,email'],
            'token' => ['required', 'max:8'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->redirect(back());
        }

        $user = User::find([
            'email:is' => $request->input('email'),
        ])[0];

        if ($user->activation_token !== $request->input('token')) {
            return $this->with('errors', [
                'token' => 'Activation failed: your token does not match.'
            ])->redirect(back());
        }

        $user->activation_token = null;
        $user->save();

        return $this->with('success', 'Your account is now activated. Please log-in.')
            ->redirect('/auth/login');
    }

    public function sendResetLink(Request $request)
    {
        $v = new Validator($request->inputs(), [
            'email' => ['required', 'email', 'exists:users,email'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())->redirect(back());
        }

        $user = User::find(['email:is' => $request->input('email')])[0];

        if (($old_reset = $user->passwordResetLink())) {
            $old_reset->delete();
        }

        $reset = new PasswordReset;
        $reset->user_id = $user->id;
        $reset->token = str_random(16);
        $reset->save();

        Mail::send($request->input('email'), 'Camagru * Password reset', 'reset', [
            'token' => $reset->token,
        ]);

        return $this->with('success', 'A password reset link has been sent to your e-mail address.')->redirect('/');
    }

    public function reset(Request $request, $token)
    {
        $reset = PasswordReset::find(['token:is' => $token]);

        if ($reset === null || empty($reset)) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $v = new Validator($request->inputs(), [
            'password' => ['required', 'min:8', 'complex'],
            'password_confirmation' => ['required', 'match:password']
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->redirect(back());
        }

        $user = $reset[0]->user();

        if (!$user) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $user->password = User::hash($request->input('password'));
        $user->save();
        $reset[0]->delete();

        return $this->with('success', 'Your password has been modified with success!')
            ->redirect(auth() ? '/' : '/auth/login');
    }
}
