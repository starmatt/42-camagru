<?php

namespace App\Controllers;

use Internals\Request;
use Internals\Controller;
use Internals\Validator;
use Internals\Session;
use App\Models\User;
use App\Models\Image;
use App\Models\Like;

class ImageController extends Controller
{
    private $upload_dir = DIR_PUBLIC . '/images/uploads';

    public function show(Request $request, $id)
    {
        if (!($image = Image::find($id))) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        return $this->view('image', [
            'image' => $image,
            'user' => $image->user(),
            'comments' => $image->comments(),
            'likes' => $image->likes(),
        ]);
    }

    public function delete(Request $request, $id)
    {
        if (!($image = Image::find($id))) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        if (!auth() || auth_user()->id !== $image->user()->id) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        if (file_exists("$this->upload_dir/$image->path")) {
            unlink("$this->upload_dir/$image->path");
        }

        $comments = $image->comments();
        $likes = $image->likes();

        foreach ($comments as $comment) {
            $comment->delete();
        }

        foreach ($likes as $like) {
            $like->delete();
        }

        $image->delete();

        return $this->with('success', 'Your picture was deleted with success.')->redirect('/');
    }

    public function edit(Request $request, $id)
    {
        $field = array_keys($request->inputs())[0];

        if (!($image = Image::find($id))) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        if (!auth() || auth_user()->id !== $image->user()->id) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $v = new Validator(array_merge($request->inputs(), ['field' => $field]), [
            'field' => ['in:title,description'],
            'title' => ['optional', 'max:80'],
            'description' => ['optional', 'max:255'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->redirect(back());
        }

        $image->{$field} = $request->input($field);
        $image->save();

        return $this->with('success', 'Your picture was modified with success!')
            ->redirect(back());
    }

    public function like(Request $request, $id)
    {
        if (!($image = Image::find($id))) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        if (guest()) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $like = Like::find([
            'image_id:is' => $id,
            'user_id:is' => auth_user()->id,
        ])[0];

        if (!$like) {
            $like = new Like;
            $like->user_id = auth_user()->id;
            $like->image_id = $id;
            $like->save();

            return $this->with('success', 'You liked ' . $image->user()->username. '\'s picture!')->redirect(back());
        }

        $like->delete();

        return $this->with('success', 'You unliked ' . $image->user()->username. '\'s picture!')->redirect(back());
    }
}
