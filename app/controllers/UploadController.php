<?php

namespace App\Controllers;

use Internals\Controller;
use Internals\Request;
use Internals\Validator;
use App\Models\Image;
use App\Models\User;

class UploadController extends Controller
{
    private $upload_dir = DIR_PUBLIC . '/images/uploads';

    private $filter_dir = DIR_PUBLIC . '/images/filters';

    public function __construct()
    {
        if (guest()) {
            return $this->redirect('/auth/login');
        }
    }

    public function show()
    {
        $user = User::current();

        return $this->view('upload', [
            'images' => $user->images(),
        ]);
    }

    public function upload(Request $request)
    {
        $v = new Validator($request->inputs(), [
            'filter' => ['required'],
            'selected' => ['required'],
            'upload' => ['file', 'type:jpeg,jpg,png'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())->redirect(back());
        }

        if (!$request->input('upload')->isUploaded() && !$request->input('webcam')) {
            return $this->with('errors', [
                'upload' => 'No pictures have been uploaded.',
            ])->redirect(back());
        }

        $filter = imagecreatefrompng($this->filter_dir . "/{$request->input('filter')}.png");

        if ($filter === false) {
            return $this->with('errors', [
                'upload' => 'Selected filter is kaput /shrug',
            ])->redirect(back());
        }

        if ($request->input('selected') === 'webcam') {
            $data = str_replace('data:image/png;base64,', '', $request->input('webcam'));
            $blob = base64_decode($data);
            $img = imagecreatefromstring($blob);
        }

        if ($request->input('selected') === 'upload') {
            $width = 640;
            $height = 480;
            $offset_x = 0;
            $offset_y = 0;

            $img = imagecreatetruecolor($width, $height);
            imagesavealpha($img, true);
            imagefill($img, 0, 0, imagecolorallocatealpha($img, 0, 0, 0, 127));

            list($source_width, $source_height) = getimagesize($request->input('upload')->tmpPath());
            $ratio = $source_width / $source_height;

            if ($width/$height > $ratio) {
                $width = $height * $ratio;
                $offset_x = (640 / 2) - ($width / 2);
            } else {
                $height = $width / $ratio;
                $offset_y = (480 / 2) - ($height / 2);
            }

            $source = imagecreatefromfile($request->input('upload')->tmpPath(), $request->input('upload')->ext());

            imagecopyresampled($img, $source, $offset_x, $offset_y, 0, 0, $width, $height, $source_width, $source_height);
            imagedestroy($source);
        }

        imagecopy($img, $filter, 0, 0, 0, 0, 640, 480);

        $filename = $this->makeFilename('png');

        imagepng($img, "$this->upload_dir/" . $filename, 0);
        imagedestroy($img);
        imagedestroy($filter);

        $img = new Image;
        $img->user_id = User::current()->id;
        $img->path = $filename;
        $img->save();

        return $this->with('success', 'Picture uploaded sucessfully!')->redirect("/picture/$img->id");
    }

    private function makeFilename($ext)
    {
        return 'image_' . User::current()->id . '-' . time() . ".$ext";
    }
}
