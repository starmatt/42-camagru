<?php

namespace App\Controllers;

use Internals\Controller;
use Internals\Request;
use Internals\Validator;
use Internals\Mail;
use App\Models\Comment;
use App\Models\User;
use App\Models\Image;

class CommentController extends Controller
{
    public function __construct()
    {
        if (guest()) {
            return $this->redirect('/auth/login');
        }
    }

    public function store(Request $request, $image_id)
    {
        $v = new Validator(array_merge($request->inputs(), ['image_id' => $image_id]), [
            'image_id' => ['required', 'exists:images,id'],
            'content' => ['required', 'max:255'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', $v->errors())
                ->with('content', $content)
                ->redirect(back());
        }

        $comment = new Comment;
        $comment->user_id = User::current()->id;
        $comment->image_id = $image_id;
        $comment->content = $request->input('content');
        $comment->save();

        $image = Image::find($image_id);
        $recipient = $image->user();

        if ($recipient->notifications) {
            Mail::send($recipient->email, 'Camagru * New comment', 'comment', [
                'username' => User::current()->username,
                'image' => $image,
                'comment' => $comment->refresh(),
            ]);
        }

        return $this->with('success', 'Your comment was added with success!')
            ->redirect(back());
    }

    public function delete(Request $request, $id)
    {
        $comment = Comment::find($id);

        if (!$comment) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        if (!auth() || auth_user()->id !== $comment->user()->id) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $comment->delete();

        return $this->with('success', 'Your comment was deleted successfully!')->redirect(back());
    }

    public function edit(Request $request, $id)
    {
        if (!($comment = Comment::find($id))) {
            header("{$request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        if (!auth() || auth_user()->id !== $comment->user()->id) {
            header("{$request->server_protocol} 401 Unauthorized");
            die('401 Unauthorized');
        }

        $v = new Validator($request->inputs(), [
            'content' => ['required', 'max:255'],
        ]);

        if (!$v->validate()) {
            return $this->with('errors', ["comment-$id" => $v->errors()['content']])->redirect(back());
        }

        $comment->content = $request->input('content');
        $comment->save();

        return $this->with('success', 'Your comment was edited successfully!')->redirect(back());
    }
}