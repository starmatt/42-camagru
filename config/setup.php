#!/usr/bin/env php
<?php

require_once __DIR__ . '/../bootstrap.php';

use Internals\Database\Connector;
use Internals\Database\Builder;

if (!isset($argv) || !array_key_exists(1, $argv)) {
    help();
    die (-1);
}

if ($argv[1] === 'help') {
    help();
    die;
}

if (in_array($argv[1], ['build', 'destroy', 'rebuild'])) {
    require_once DIR_CONF . '/schema.php';

    $builder = new Builder(new Connector);
    $argv[1]($builder);
    die;
}

echo "Unknown command '$argv[1]'." . PHP_EOL;
help();
die (-1);

function help()
{
    echo 'Available commands: build, destroy, rebuild, help.' . PHP_EOL;
}

function build($builder)
{
    up($builder);
}

function destroy($builder)
{
    down($builder);
}

function rebuild($builder)
{
    down($builder);
    up($builder);
}
