<?php

use Internals\Database\TableMaker;

function create($builder)
{
    $builder->createDatabase();
}

function up($builder)
{
    /**
     * Users table
     */
    $builder->createTable('users', function (TableMaker $table) {
        $table->primary('id');
        $table->string('email', 80, [
            'unique' => true,
        ]);
        $table->string('password');
        $table->string('username', 80, [
            'unique' => true,
        ]);
        $table->boolean('notifications', [
            'default' => 1,
        ]);
        $table->string('activation_token', 8, [
            'nullable' => true,
        ]);
        $table->timestamps();
    });

    /**
     * Pictures table
     */
    $builder->createTable('images', function (TableMaker $table) {
        $table->primary('id');
        $table->integer('user_id', 11, [
            'signed' => false
        ]);
        $table->string('title', 80, [
            'nullable' => true,
        ]);
        $table->string('path');
        $table->text('description', [
            'nullable' => true,
        ]);
        $table->timestamps();
    });

    /**
     * Comments table
     */
    $builder->createTable('comments', function (TableMaker $table) {
        $table->primary('id');
        $table->integer('user_id', 11, [
            'signed' => false
        ]);
        $table->integer('image_id', 11, [
            'signed' => false,
        ]);
        $table->integer('comment_id', 11, [
            'signed' => false,
            'nullable' => true,
        ]);
        $table->string('content');
        $table->timestamps();
    });

    /**
     * Likes table
     */
    $builder->createTable('likes', function (TableMaker $table) {
        $table->primary('id');
        $table->integer('user_id', 11, [
            'signed' => false
        ]);
        $table->integer('image_id', 11, [
            'signed' => false
        ]);
        $table->timestamps();
    });

    /**
     * Password reset keys table
     */
    $builder->createTable('password_resets', function (TableMaker $table) {
        $table->primary('id');
        $table->integer('user_id', 11, [
            'signed' => false
        ]);
        $table->string('token', 16);
        $table->timestamps();
    });
}

function down($builder)
{
    $builder->dropTable('users');
    $builder->dropTable('images');
    $builder->dropTable('comments');
    $builder->dropTable('likes');
    $builder->dropTable('password_resets');
}
