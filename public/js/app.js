// Toggle the navigation menu
var menuBtns = document.getElementsByClassName('menu-btn');

if (menuBtns) {
    var open = false;
    var mobileNav = document.getElementById('mobile-nav');
    var body = document.getElementsByTagName('body')[0];

    for (var i = 0; i < menuBtns.length; i++) {
        menuBtns[i].addEventListener('click', function () {
            mobileNav.classList.toggle('open');
            body.classList.toggle('nav-open');
            open = !open;
        });
    }

    // Close the menu when clicking outside
    document.addEventListener('click', function (e) {
        if (
            !mobileNav.contains(e.target)
            && !e.target.parentNode
            && !e.target.parentNode.classList.contains('menu-btn')
            && open
        ) {
            mobileNav.classList.remove('open');
            body.classList.remove('nav-open');
            open = false;
        }
    })
}

// Handle the upload page events
var uploadClick = document.getElementById('upload-click');
var webcamClick = document.getElementById('webcam-click');

if (uploadClick && webcamClick) {
    var divUpload = document.getElementById('upload-wrapper');
    var divWebcam = document.getElementById('webcam-wrapper');
    var video = document.getElementById('stream');
    var uploadBtns = document.getElementsByClassName('upload-btn');
    var form = document.getElementById('upload-form');
    var loading = false;
    var selected = 'upload';
    var webcamError = false;
    var browserCheck = false;

    // Switch between file upload and webcam
    uploadClick.addEventListener('click', function(e) {
        if (selected === 'webcam') {
            stopStream();
            uploadClick.classList.add('selected');
            webcamClick.classList.remove('selected');
            divUpload.classList.remove('hidden');
            divWebcam.classList.add('hidden');
            selected = 'upload';
        }
    });

    webcamClick.addEventListener('click', function () {
        if (selected === 'upload') {
            startStream();
            webcamClick.classList.add('selected');
            uploadClick.classList.remove('selected');
            divWebcam.classList.remove('hidden');
            divUpload.classList.add('hidden');
            selected = 'webcam';
        }
    });

    // Handle the webcam stream
    var promisedGetUserMedia = function(constraints) {
        var getUserMedia = (
            navigator.getUserMedia
            || navigator.webkitGetUserMedia
            || navigator.mozGetUserMedia
            || navigator.msGetUserMedia
            || navigator.oGetUserMedia
        );

        return new Promise(function(resolve, reject) {
            getUserMedia.call(navigator, constraints, resolve, reject);
        });
    }

    if (!navigator.mediaDevices) {
        navigator.mediaDevices = {};
        navigator.mediaDevices.getUserMedia = promisedGetUserMedia;
    }

    if (video && navigator.mediaDevices.getUserMedia) {
        var divStream = document.getElementById('stream-preview');

        function startStream() {
            navigator.mediaDevices.getUserMedia({
                audio: false,
                video: true
            }).then(function(stream) {
                if (browserCheck || video.srcObject === undefined) {
                    video.src = window.URL.createObjectURL(stream);
                    browserCheck = true;
                }

                var status = document.getElementById('webcam-status');
                status.innerHTML = '';
                status.classList.add('hidden');
                webcamError = false;
                video.srcObject = stream;
                video.classList.remove('hidden');
            }).catch(function(error) {
                var status = document.getElementById('webcam-status');
                status.innerHTML = 'Webcam access denied: ' + error.message;
                status.classList.remove('hidden');
                webcamError = true;
            });
        }

        function stopStream() {
            if (selected === 'webcam') {
                if (!webcamError && (video.src || video.srcObject)) {
                    video.srcObject.getTracks()[0].stop();
                    video.srcObject = null;
                    video.classList.add('hidden');
                }
            }
        }
    }

    // Get the name of the file to be uploaded and show a preview
    var fileInput = document.getElementById('upload-input');
    var clear = document.getElementById('clear-upload');
    
    if (fileInput && clear) {
        var uploadLabel = document.getElementById('label-upload');
        var preview = document.getElementById('preview');

        fileInput.addEventListener('change', function () {
            var file = fileInput.files[0];
            var reader = new FileReader(file);
            preview.classList.remove('portrait', 'landscape');

            if (file.name.length) {
                uploadLabel.innerHTML = `Selected: ${file.name}`; 
                clear.classList.remove('hidden');
                reader.readAsDataURL(file);
                reader.onload = function (e) {
                    preview.src = e.target.result;
                    preview.classList.remove('hidden');
                    preview.classList.add(preview.naturalWidth > preview.naturalHeight ? 'landscape' : 'portrait');
                };
            } else {
                uploadLabel.innerHTML = `Select an image`;
                clear.classList.add('hidden');
            }
        });

        // Clear the upload input
        clear.addEventListener('click', function() {
            fileInput.value = '';
            preview.src = '';
            preview.classList.add('hidden');
            preview.classList.remove('portrait', 'landscape');
            uploadLabel.innerHTML = `Select an image`;
            clear.classList.add('hidden');
        });
    }

    // Show overlay
    var filters = document.getElementsByClassName('radio-filter');
    var overlays = document.getElementsByClassName('filter-img');
    var currentFilter = null;

    if (filters && overlays) {
        for (var i = 0; i < filters.length; i++) {
            filters[i].addEventListener('change', function (e) {
                currentFilter = e.target.value;

                for (var i = 0; i < filters.length; i++) {
                    if (e.target === filters[i]) {
                        filters[i].parentNode.classList.add('selected');
                    } else {
                        filters[i].parentNode.classList.remove('selected');
                    }
                }

                for (var i = 0; i < overlays.length; i++) {
                    overlays[i].src = `/public/images/filters/${e.target.value}.png`;
                    overlays[i].classList.remove('hidden');
                }

                if (currentFilter && !loading) {
                    for (var i = 0; i < uploadBtns.length; i++) {
                        uploadBtns[i].disabled = false;
                    }
                }
            });
        }
    }

    if (form && uploadBtns) {
        for (var i = 0; i < uploadBtns.length; i++) {
            uploadBtns[i].addEventListener('click', function (e) {
                loading = true;
                uploadBtns[i].disabled = true;
                uploadBtns[i].innerHTML = 'Loading...';
                form[0].value = currentFilter;
                form[1].value = selected;

                if (selected === 'webcam' && video.srcObject) {
                    var canvas = document.getElementById('canvas');
                    var screen = canvas.getContext('2d');

                    screen.translate(640, 0);
                    screen.scale(-1, 1);
                    screen.drawImage(video, 0, 0, 640, 480);
                    form[3].value = canvas.toDataURL('image/png');
                    form[2].value = '';
                }

                form.submit();
            });
        }
    }
}

// Handle image removal event
var imageDelete = document.getElementById('image-delete');

if (imageDelete) {
    imageDelete.addEventListener('submit', function (e) {
        var check = confirm('Are you sure you want to delete this picture?');

        if (!check) {
            e.preventDefault();
        }
    });
}

// Handle edit buttons on various UI elements
var editButtons = document.getElementsByClassName('edit-button');

if (editButtons) {
    for (var i = 0; i < editButtons.length; i++) {
        editButtons[i].open = false;
        editButtons[i].editForm = document.getElementById(editButtons[i].dataset.form);

        editButtons[i].addEventListener('click', function (e) {
            e.preventDefault();

            if (e.target.open === false) {
                e.target.classList.remove('lnr-pencil');
                e.target.classList.add('lnr-cross');
            } else {
                e.target.classList.remove('lnr-cross');
                e.target.classList.add('lnr-pencil');
            }

            e.target.open = !e.target.open;
            e.target.editForm.classList.toggle('hidden');
        });
    }
}

// Handle comment removal event
var commentsDelete = document.getElementsByClassName('comment-delete');

if (commentsDelete) {
    for (var i = 0; i < commentsDelete.length; i++) {
        commentsDelete[i].addEventListener('submit', function (e) {
            var check = confirm('Are you sure you want to delete this comment?');

            if (!check) {
                e.preventDefault();
            }
        });
    }
}

// Create a contextual form to handle comment edition
var commentsEdit = document.getElementsByClassName('comment-edit');

if (commentsEdit) {
    for (var i = 0; i < commentsEdit.length; i++) {
        commentsEdit[i].open = false;
        commentsEdit[i].commentID = commentsEdit[i].dataset.comment;
        commentsEdit[i].commentDiv = document.getElementById('comment-' + commentsEdit[i].commentID);
        commentsEdit[i].commentForm = null;

        commentsEdit[i].addEventListener('click', function (e) {
            e.preventDefault();

            if (e.target.open === false) {
                e.target.classList.remove('lnr-pencil');
                e.target.classList.add('lnr-cross');

                if (e.target.commentForm === null) {
                    e.target.commentForm = createEditForm(e.target.commentID, e.target.commentDiv.textContent, e.target.commentDiv.parentNode);
                } else {
                    e.target.commentForm.classList.remove('hidden');
                }
            } else {
                e.target.classList.remove('lnr-cross');
                e.target.classList.add('lnr-pencil');
                e.target.commentForm.classList.add('hidden');
            }

            e.target.open = !e.target.open;
            e.target.commentDiv.classList.toggle('hidden');
        });
    }

    // Create and append form when the edit button is clicked
    function createEditForm(id, comment, parent)
    {
        var form = document.createElement('form');
        form.action = `/comment/edit/${id}`;
        form.method = 'POST';
        form.id = `comment-edit-${id}`;

        var input = document.createElement('input');
        input.type = 'text';
        input.name = 'content';
        input.value = comment.trim();
        input.classList.add('mb-10');

        var button = document.createElement('button');
        button.classList.add('btn');
        button.innerHTML = 'Edit';

        form.appendChild(input);
        form.appendChild(button);
        parent.appendChild(form);

        return form;
    }
}
