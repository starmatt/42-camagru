<?php

require_once __DIR__ . '/bootstrap.php';

$router = new Internals\Router(new Internals\Request);
$router->trigger();
