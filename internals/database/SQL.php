<?php

namespace Internals\Database;

class SQL
{
    const END_STATEMENT = ';';

    const MODES = [
        'select',
        'count',
        'insert',
        'delete',
        'update'
    ];

    private $mode;

    private $statement;

    private $constraints = [];

    private $settings = [];

    public function __construct($mode, ...$args)
    {
        if (in_array($mode, self::MODES)) {
            $this->mode = $mode;
            $this->{$mode}(...$args);
        } else {
            throw new \Exception("Unknown SQL mode: $mode");
        }
    }

    private function select($table, $fields = [])
    {
        $fields = empty($fields) ? '*' : implode(', ', $fields);

        $this->statement = "SELECT $fields FROM $table";

        return $this;
    }

    private function insert($table, $fields = [])
    {
        $placeholder = rtrim(str_repeat('?, ', count($fields)), ', ');
        $fields = implode(', ', array_keys($fields));

        $this->statement = "INSERT INTO $table ($fields) VALUES ($placeholder)";

        return $this;
    }

    private function delete($table, $primary_key)
    {
        $this->statement = "DELETE FROM $table WHERE $primary_key = ?";
    }

    private function update($table, $fields)
    {
        $tmp = [];

        foreach (array_keys($fields) as $field) {
            $tmp[] = "`$field` = ?";
        }

        $fields = implode(', ', $tmp);

        $this->statement = "UPDATE $table SET $fields";

        return $this;
    }

    private function count($table)
    {
        $this->statement = "SELECT COUNT(*) FROM $table";
    }

    public function addConstraint($key, $value, $operator = '=')
    {
        if (count($this->constraints) > 0) {
            $this->constraints[] = "AND `$key` $operator '$value'";
        } else {
            $this->constraints[] = "WHERE `$key` $operator '$value'";
        }

        return $this;
    }

    public function addSetting($setting, $value)
    {
        $this->settings[$setting] = $value;

        return $this;
    }

    public function get()
    {
        if (!empty($this->constraints)) {
            $this->statement .= ' ' . implode(' ', $this->constraints);
        }

        foreach ($this->settings as $setting => $value) {
            $this->statement .= " $setting $value";
        }

        return $this->statement . self::END_STATEMENT;
    }

    public function __toString()
    {
        return $this->get();
    }
}
