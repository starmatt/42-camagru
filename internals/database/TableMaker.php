<?php

namespace Internals\Database;

class TableMaker
{
    private $default_modifiers = [
        'zerofill' => false,
        'signed' => true,
        'nullable' => false,
        'default' => null,
        'default_expr' => null,
        'default_const' => null,
        'unique' => false,
        'on_update' => null,
    ];

    private $is_string = false;

    public $columns = [];

    /**
     * String data-types
     */
    public function string($name, $size = 255, $options = [])
    {
        $this->data_type = 'string';
        $this->columns[$name] = $this->applyMods("$name VARCHAR($size)", $options);
    }

    public function char($name, $size = 1, $options = [])
    {
        $this->data_type = 'string';
        $this->columns[$name] = $this->applyMods("$name CHAR($size)", $options);
    }

    public function text($name, $options = [])
    {
        $this->data_type = 'string';
        $this->columns[$name] = $this->applyMods("$name TEXT", $options);
    }

    /**
     * Numeric data-types
     */
    public function integer($name, $size = 11, $options = [])
    {
        $this->data_type = 'numeric';
        $this->columns[$name] = $this->applyMods("$name INTEGER($size)", $options);
    }

    public function double($name, $options = [])
    {
        $this->data_type = 'numeric';
        $this->columns[$name] = $this->applyMods("$name DOUBLE", $options);
    }

    public function boolean($name, $options = [])
    {
        $this->data_type = 'numeric';
        $this->columns[$name] = $this->applyMods("$name BOOLEAN", $options);
    }

    /**
     * Date data-types
     */
    public function timestamp($name, $options = [])
    {
        $this->data_type = 'date';
        $this->columns[$name] = $this->applyMods("$name TIMESTAMP", $options);
    }

    /**
     * Others
     */
    public function primary($name, $size = 11)
    {
        $this->data_type = 'numeric';
        $this->columns[$name] = "$name INT($size) UNSIGNED AUTO_INCREMENT PRIMARY KEY";
    }

    public function timestamps()
    {
        $this->timestamp('created_at', [
            'default_const' => 'CURRENT_TIMESTAMP',
        ]);

        $this->timestamp('updated_at', [
            'nullable' => true,
            'on_update' => 'CURRENT_TIMESTAMP',
        ]);
    }

    public function custom($name, $datatype, $size = null, $options = [])
    {
        $sql = "$name $datatype";
        $sql .= $size ? "($size)" : '';

        $this->columns[$name] = $this->applyMods($sql, $options);
    }

    public function __toString()
    {
        return implode(','.PHP_EOL, $this->columns);
    }

    public function count()
    {
        return count($this->columns);
    }

    private function applyMods($sql, $options)
    {
        $modifiers = array_merge($this->default_modifiers, $options); 

        foreach ($modifiers as $modifier => $value) {
            switch ($modifier) {
                case 'signed':
                    if ($this->data_type === 'numeric') $sql .= $value === true ? '' : ' UNSIGNED';
                    break;
                case 'zerofill':
                    if ($this->data_type === 'numeric') $sql .= $value === false ? '' : ' ZEROFILL';
                    break;
                case 'nullable':
                    $sql .= $value === false ? ' NOT NULL' : ' NULL';
                    break;
                case 'default_expr':
                    $sql .= $value === null ? '' : " DEFAULT ($value)";
                    break;
                case 'default_const':
                    $sql .= $value === null ? '' : " DEFAULT $value";
                    break;
                case 'default':
                    $sql .= $value === null ? '' : " DEFAULT '$value'";
                    break;
                case 'unique':
                    $sql .= $value === false ? '' : " UNIQUE";
                    break;
                case 'on_update':
                    $sql .= $value === null ? '' : " ON UPDATE $value";
                    break;
                default:
                    break;
            }
        }

        return $sql;
    }
}
