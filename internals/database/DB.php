<?php

namespace Internals\Database;

use Internals\Database\Connector;
use Internals\Database\SQL;

class DB
{
    private $connection;

    private $operators = [
        'is' => '=',
        'like' => 'LIKE',
        'gt' => '>',
        'lt' => '<',
        'gte' => '>=',
        'lte' => '<=',
    ];

    private $settings = [
    ];

    public function __construct(Connector $connector)
    {
        $this->connection = $connector->connection();
    }

    public function save($model)
    {
        return $model->index() === null
            ? $this->insert($model)
            : $this->update($model);
    }

    public function find($model, $id, $fields = [])
    {
        $sql = new SQL('select', $model->getTable(), $model->checkAttributes($fields));
        $sql->addConstraint($model->getPrimaryKey(), $id);

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute([$id]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt = null;

        return $result;
    }

    public function findWithConstraints($model, $constraints, $fields = [], $settings)
    {
        $sql = new SQL('select', $model->getTable(), $model->checkAttributes($fields));

        foreach ($constraints as $constraint => $value) {
            $constraint = explode(':', $constraint);

            if (!isset($this->operators[$constraint[1]])) {
                throw new \Exception("Unknown operator: $constraint[1]");
            }

            $sql->addConstraint($constraint[0], $value, $this->operators[$constraint[1]]);
        }

        foreach ($settings as $setting => $value) {
            $sql->addSetting($setting, $value);
        }

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt = null;

        return $results;
    }

    public function all($model)
    {
        $sql = new SQL('select', $model->getTable());

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt = null;

        return $results === false ? null : $results;
    }

    public function delete($model)
    {
        $sql = new SQL('delete', $model->getTable(), $model->getPrimaryKey());

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute([$model->index()]);
        $stmt = null;
    }

    public function related($model, $foreign_table, $foreign_key)
    {
        $sql = new SQL('select', $foreign_table);
        $sql->addConstraint($foreign_key, $model->index());

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt = null;

        return $results;
    }

    public function parent($model, $parent_model, $parent_key)
    {
        $sql = new SQL('select', $parent_model->getTable());
        $sql->addConstraint($parent_model->getPrimaryKey(), $model->{$parent_key});

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute();
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $stmt = null;

        return $results;
    }

    public function count($model)
    {
        $sql = new SQL('count', $model->getTable());

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute();
        $count = $stmt->fetch();

        return $count[0];
    }

    private function insert($model)
    {
        $fields = $model->getModifiableAttributesAssoc();
        $values = array_values($fields);
        $sql = new SQL('insert', $model->getTable(), $fields);

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute($values);
        $stmt = null;

        return $this->connection->lastInsertId();
    }

    private function update($model)
    {
        $fields = $model->getModifiableAttributesAssoc();
        $values = array_values($fields);
        $sql = new SQL('update', $model->getTable(), $fields);
        $sql->addConstraint($model->getPrimaryKey(), $model->index());

        $stmt = $this->connection->prepare($sql->get());
        $stmt->execute($values);
        $stmt = null;
    }
}
