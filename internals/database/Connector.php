<?php

namespace Internals\Database;

use \PDO;

class Connector
{
    const CONF_DEFAULT = DIR_CONF . '/database.php';

    private $pdo;

    public function __construct($conf = null)
    {
        require $conf && file_exists($conf) ? $conf : self::CONF_DEFAULT;

        $this->pdo = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ]);
    }

    public function disconnect()
    {
        $this->pdo = null;
    }

    public function connection()
    {
        return $this->pdo;
    }

    public function run($sql)
    {
        return $this->pdo->exec($sql);
    }

    public function errors()
    {
        return $this->pdo->errorInfo();
    }

    public function __destruct()
    {
        $this->disconnect();
    }
}
