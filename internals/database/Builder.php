<?php

namespace Internals\Database;

use Internals\Database\Connector;
use Internals\Database\TableMaker;

class Builder
{
    private $connection;

    public function __construct(Connector $connector)
    {
        $this->connection = $connector;
    }

    public function createTable($name, \Closure $callback)
    {
        $table = new TableMaker();
        call_user_func($callback, $table);

        $statement = $this->createTableTemplate($name, (string) $table);

        $this->connection->run($statement);
    }

    public function dropTable($name)
    {
        $statement = $this->dropTableTemplate($name);

        $this->connection->run($statement);
    }

    private function createDBTemplate($name)
    {
        
    }

    private function createTableTemplate($name, $columns)
    {
        return "CREATE TABLE IF NOT EXISTS $name ($columns);";
    }

    private function dropTableTemplate($name)
    {
        return "DROP TABLE IF EXISTS $name;";
    }
}
