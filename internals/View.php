<?php

namespace Internals;

class View
{
    private static $layout = DIR_VIEWS . "/layouts/camagru.php";

    public static function render($view, $data)
    {
        ob_start();

        extract($data);

        require_once self::$layout ?: $view;

        return ob_get_clean();
    }
}
