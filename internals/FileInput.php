<?php

namespace Internals;

class FileInput
{
    private $name;

    private $type;

    private $size;

    private $tmp_name;

    private $error_code;

    private $is_file = false;

    private $stored_path;

    public function __construct(array $file)
    {
        $this->is_file = $this->validate($file);

        $this->name = $file['name'];
        $this->type = $file['type'];
        $this->size = $file['size'];
        $this->tmp_name = $file['tmp_name'];
        $this->error_code = $file['error'];
    }

    private function validate($input)
    {
        if (!is_array($input)) {
            return false;
        }

        if (!array_keys_exist(['name', 'type', 'tmp_name', 'error', 'size'], $input)) {
            return false;
        }

        if (strlen($input['tmp_name']) > 0 && !is_file($input['tmp_name'])) {
            return false;
        }

        return true;
    }

    public function isFile()
    {
        return $this->is_file;
    }

    public function isUploaded()
    {
        return $this->error_code === 0;
    }

    public function error()
    {
        return $this->error_code;
    }

    public function name()
    {
        return $this->name;
    }

    public function size()
    {
        return $this->size;
    }

    public function ext()
    {
        return pathinfo($this->name, PATHINFO_EXTENSION);
    }

    public function filename()
    {
        return pathinfo($this->name, PATHINFO_FILENAME);
    }

    public function storedPath()
    {
        return $this->stored_path;
    }

    public function tmpPath()
    {
        return $this->tmp_name;
    }

    public function store($dir, $name)
    {
        if (!$this->is_file || !$this->error_code === 0 || $this->stored_path !== null) {
            return false;
        }

        $path = "$dir/$name";

        if (@move_uploaded_file($this->tmp_name, $path)) {
            $this->stored_path = $path;
        }

        return $this->stored_path;
    }
}
