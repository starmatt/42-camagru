<?php

namespace Internals;

class Session
{
    static private $lifetime = 86400;

    private static function start()
    {
        if (session_status() === PHP_SESSION_DISABLED) {
            throw new Exception('PHP Sessions are disabled.');
        }

        if (session_status() === PHP_SESSION_NONE) {
            session_start();

            if (($time = self::time()) !== null) {
                if ((time() - $time) > self::$lifetime) {
                    self::reset();
                    self::store('session_created', time());
                }
            } else {
                self::store('session_created', time());
            }
        }
    }

    public static function store($key, $value)
    {
        self::start();
        $_SESSION[$key] = $value;

        return $value;
    }

    public static function flash($key, $value = '')
    {
        self::start();

        if (self::has($key)) {
            $value = self::get($key);
            self::delete($key);

            return $value;
        }

        self::store($key, $value);
    }

    public static function get($key)
    {
        self::start();
        return self::has($key) ? $_SESSION[$key] : null;
    }

    public static function has($key)
    {
        self::start();
        return array_key_exists($key, $_SESSION);
    }

    public static function delete($key)
    {
        self::start();

        if (self::has($key)) {
            unset($_SESSION[$key]);
        }
    }

    public static function id()
    {
        self::start();
        return session_id();
    }

    public static function all()
    {
        self::start();
        return $_SESSION;
    }

    public static function reset()
    {
        session_unset();
        session_destroy();
        self::start();
        session_regenerate_id(true);
    }

    private static function time()
    {
        return self::get('session_created');
    }
}
