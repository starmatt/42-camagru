<?php

namespace Internals;

use Internals\Database\DB;
use Internals\Database\Connector;

abstract class Model
{
    protected $db;

    protected $table;

    protected $primary_key = 'id';

    protected $index;

    protected $timestamps = ['created_at', 'updated_at'];

    protected $attributes = [];

    protected $relationships = [];

    public function __construct()
    {
        $this->db = new DB(new Connector);

        foreach ($this->getAllAttributes() as $attribute) {
            $this->{$attribute} = null;
        }
    }

    public function save()
    {
        $id = $this->db->save($this);

        if ($id !== null) {
            $this->index = $id;
            $this->{$this->primary_key} = $id;
        }
    }

    public function delete()
    {
        $this->db->delete($this);
        $this->selfDestruct();
    }

    public static function find($var, $select = [], $settings = [])
    {
        $class = static::class;

        if (is_array($var) || $var === '*') {
            $rows = ($model = new $class)->db->findWithConstraints(
                $model,
                ($var === '*' ? [] : $var),
                $select,
                $settings
            );

            if (empty($rows)) {
                return null;
            }

            $data = [];

            foreach ($rows as $row) {
                $data[] = (new $class)->fill($row);
            }

            return $data;
        }

        $data = ($model = new $class)->db->find($model, $var, $select);

        return $data === false ? null : $model->fill($data);
    }

    public static function all()
    {
        $class = static::class;
        $data = [];

        foreach (($model = new $class)->db->all($model) as $result) {
            $data[] = (new $class)->fill($result);
        }

        return $data;
    }

    public static function count()
    {
        $class = static::class;

        return ($model = new $class)->db->count($model);
    }

    public function refresh()
    {
        return self::find($this->index());
    }

    public function fill($attributes)
    {
        foreach ($attributes as $attribute => $value) {
            if ($attribute === $this->primary_key) {
                $this->index = $value;
                $this->{$this->primary_key} = $value;
            } else if (property_exists($this, $attribute)) {
                $this->{$attribute} = $value;
            }
        }

        return $this;
    }

    public function checkAttributes($fields)
    {
        return array_filter($fields, function ($field) {
            return in_array($field, $this->getAllattributes());
        });
    }

    public function getModifiableAttributes()
    {
        return $this->attributes;
    }

    public function getModifiableAttributesAssoc()
    {
        $attributes = [];

        foreach ($this->getModifiableAttributes() as $attribute) {
            $attributes[$attribute] = $this->{$attribute};
        }

        return $attributes;
    }

    public function getAllAttributes()
    {
        $attributes = $this->attributes;
        array_unshift($attributes, $this->primary_key);

        if ($this->timestamps && is_array($this->timestamps)) {
            $attributes = array_merge($attributes, $this->timestamps);
        }

        return $attributes;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getPrimaryKey()
    {
        return $this->primary_key;
    }

    public function index()
    {
        return $this->index;
    }

    public function rel($key)
    {
        return array_key_exists($key, $this->relationships) ? $this->relationships[$key] : null;
    }

    private function selfDestruct()
    {
        foreach ($this->getAllAttributes() as $attribute) {
            $this->{$attribute} = null;
        }

        return $this;
    }

    protected function hasOne($related_model, $foreign_key)
    {
        $relation_name = debug_backtrace()[1]['function'];

        if (array_key_exists($relation_name, $this->relationships)) {
            return $this->relationships[$relation_name];
        }

        $related_table = (new $related_model)->getTable();
        $rows = $this->db->related($this, $related_table, $foreign_key);

        if (!empty($rows)) {
            $this->relationships[$relation_name] = (new $related_model)->fill($rows[0]);

            return $this->relationships[$relation_name];
        }

        return null;
    }

    protected function hasMany($related_model, $foreign_key)
    {
        $relation_name = debug_backtrace()[1]['function'];

        if (array_key_exists($relation_name, $this->relationships)) {
            return $this->relationships[$relation_name];
        }

        $related_table = (new $related_model)->getTable();
        $rows = $this->db->related($this, $related_table, $foreign_key);

        if (!empty($rows)) {
            foreach ($rows as $row) {
                $this->relationships[$relation_name][] = (new $related_model)->fill($row);
            }

            return $this->relationships[$relation_name];
        }

        return [];
    }

    protected function belongsTo($parent_model, $parent_key)
    {
        $relation_name = debug_backtrace()[1]['function'];

        if (array_key_exists($relation_name, $this->relationships)) {
            return $this->relationships[$relation_name];
        }

        $rows = $this->db->parent($this, (new $parent_model), $parent_key);

        if (!empty($rows)) {
            $this->relationships[$relation_name] = (new $parent_model)->fill($rows[0]);

            return $this->relationships[$relation_name];
        }

        return null;
    }
}
