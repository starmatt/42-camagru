<?php

namespace Internals;

use Internals\Request;

class Router
{
    /**
     * Allowed methods
     */
    private $allowed_methods = [
        'GET',
        'POST',
    ];

    /**
     * A list of all available routes
     */
    private $routes;

    /**
     * The request's URI
     */
    private $uri;

    /**
     *  An object representing the current server request
     */
    private $request;

    /**
     * An object representing the controller that will handle the request
     */
    private $controller;

    /**
     * The name of the method that will handle the request
     */
    private $action;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->routes = require_once DIR_ROOT . '/routes.php';
        $this->uri = $this->cleanURI();
    }

    public function trigger()
    {
        $route = $this->validateURI();
        $parameters = $this->getParameters($route);

        $this->controller = new $route['controller'];
        $this->action = $route['action'];

        return $this->controller->{$this->action}($this->request, ...$parameters);
    }

    private function cleanURI()
    {
        $uri = explode('?', $this->request->request_uri)[0];

        return strlen($uri) > 1 && substr($uri, -1) === '/' ? rtrim($uri, '/') : $uri;
    }

    private function validateURI()
    {
        $route = $this->getRouteFromList();

        if (!$route) {
            header("{$this->request->server_protocol} 404 Not Found");
            die('404 Not Found');
        }

        if (!in_array($route['method'], $this->allowed_methods)
            || $this->request->request_method !== $route['method'])
        {
            header("{$this->request->server_protocol} 405 Method not allowed");
            die('405 Method not allowed');
        }

        return $route;
    }

    private function getRouteFromList()
    {
        foreach ($this->routes as $route) {
            if (preg_match($this->makePatternFromURI($route['uri']), $this->uri)
                && $this->request->request_method === $route['method'])
            {
                return $route;
            }
        }
    }

    private function makePatternFromURI($uri)
    {
        if (strlen($uri) === 1) {
            return '/^.$/';
        }

        $pattern = '/^';
        $parts = array_filter(explode('/', $uri));

        foreach ($parts as $part) {
            if ($part[0] === '{' && $part[strlen($part) - 1] === '}') {
                $pattern .= '(\/\w+?)';
            } else {
                $pattern .= "(\/$part)";
            }
        }

        return "$pattern$/";
    }

    private function getParameters($route)
    {
        $parameters = [];
        $route_uri_parts = explode('/', $route['uri']);
        $request_uri_parts = explode('/', $this->uri);

        foreach ($route_uri_parts as $key => $part) {
            if ($part && $part[0] === '{' && $part[strlen($part) - 1] === '}') {
                $parameters[] = $request_uri_parts[$key];
            }
        }

        return $parameters;
    }
}
