<?php

namespace Internals\Auth;

use Internals\Model;
use Internals\Session;

class Authenticable extends Model
{
    public function verify($value, $hash = 'password')
    {
        return password_verify($value, $this->{$hash});
    }

    public static function hash($value)
    {
        $hashed = password_hash($value, PASSWORD_DEFAULT);

        return $hashed;
    }

    public function authenticate($password)
    {
        if (auth()) {
            return true;
        }

        if ($this->verify($password)) {
            Session::store('auth_id', $this->{$this->primary_key});

            return true;
        }

        return false;
    }

    public function is_active()
    {
        return $this->activation_token === null;
    }

    public static function logout()
    {
        Session::reset();
    }

    public static function current()
    {
        if (auth()) {
            return self::find(Session::get('auth_id'));
        }
    }
}
