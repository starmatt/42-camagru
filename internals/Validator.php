<?php

namespace Internals;

use Internals\Database\Connector;
use Internals\Database\SQL;

class Validator
{
    private $db;

    private $errors = [];

    private $rules = [];

    private $inputs = [];

    private $allowed_rules = [
        'required',
        'unique',
        'exists',
        'max',
        'min',
        'match',
        'email',
        'file',
        'in',
        'complex',
    ];

    public function __construct(array $inputs, array $rules)
    {
        $this->inputs = $inputs;
        $this->rules = $rules;
    }

    public function validate()
    {
        foreach ($this->rules as $input => $rules) {
            if (($key = array_search('optional', $rules)) !== false) {
                if (!in_array($input, array_keys($this->inputs))) {
                    continue;
                }

                unset($rules[$key]);
            }

            if (($key = array_search('file', $rules)) !== false) {
                unset($rules[$key]);

                foreach ($rules as $key => $rule) {
                    $tmp = explode(':', $rule);
                    $rules[$tmp[0]] = isset($tmp[1]) ? $tmp[1] : null;
                    unset($rules[$key]);
                }

                $this->validateFile($input, $rules);
                break;
            }

            foreach ($rules as $rule) {
                dump($rule);
                $rule = explode(':', $rule);
                $method = $rule[0];
                $value = isset($rule[1]) ? $rule[1] : null;

                if (!in_array($method, $this->allowed_rules)) {
                    throw new \Exception("Cannot validate rule: $method, unknown method.");
                }

                if (!$this->{$method}($input, $value)) {
                    break;
                }
            }
        }

        return empty($this->errors);
    }

    public function errors()
    {
        return $this->errors;
    }

    private function validateFile($name, $rules)
    {
        $file = $this->inputs[$name];

        $undefined = [
            UPLOAD_ERR_PARTIAL,
            UPLOAD_ERR_NO_TMP_DIR,
            UPLOAD_ERR_CANT_WRITE,
            UPLOAD_ERR_EXTENSION
        ];

        if (!$file->isFile()) {
            $this->errors[$name] = "The value for '$name' is not a file.";

            return;
        }

        if ($file->error() === UPLOAD_ERR_INI_SIZE) {
            $size_limit = ini_get('upload_max_filesize');
            $this->errors[$name] = "The value for '$name' exceeds the maximum size limit of $size_limit.";

            return;
        }

        if (in_array($file->error(), $undefined)) {
            $this->errors[$name] = "An error ($file[error]) occured when trying to upload your file. Try again later.";

            return;
        }

        if (array_key_exists('required', $rules) && $file->error() === UPLOAD_ERR_NO_FILE) {
            $this->errors[$name] = "The '$name' field is required.";

            return;
        }

        if (array_key_exists('type', $rules) && $file->isUploaded()) {
            $ext = pathinfo($file->name(), PATHINFO_EXTENSION);
            $types = explode(',', $rules['type']);

            if (!in_array($ext, $types)) {
                $types = implode(', ', $types);
                $this->errors[$name] = "Invalid file type for '$name': accepted extensions are $types.";

                return;
            }
        }
    }

    private function required($name)
    {
        if (!array_key_exists($name, $this->inputs)
            || $this->inputs[$name] === null
            || $this->inputs[$name] === ''
        ) {
            $this->errors[$name] = "The '$name' field is required.";

            return false;
        } 

        return true;
    }

    private function unique($name, $value)
    {
        $sql = (new SQL('select', $value, ['id']))->addConstraint($name, $this->inputs[$name]);
        $stmt = (new Connector)->connection()->prepare($sql->get());
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->errors[$name] = "The value for '$name' already exists in our database.";
            $stmt = null;

            return false;
        }

        $stmt = null;

        return true;
    }

    private function max($name, $value)
    {
        $value = (int) $value;

        if (strlen($this->inputs[$name]) > $value) {
            $this->errors[$name] = "The '$name' field must be at most $value characters.";

            return false;
        }

        return true;
    }

    private function min($name, $value)
    {
        $value = (int) $value;

        if (strlen($this->inputs[$name]) < $value) {
            $this->errors[$name] = "The '$name' field must be at least $value characters.";

            return false;
        }

        return true;
    }

    private function match($name, $value)
    {
        if ($this->inputs[$name] !== $this->inputs[$value]) {
            $this->errors[$name] = "The '$name' field doesn't match the $value field.";

            return false;
        }

        return true;
    }

    private function email($name)
    {
        if (!filter_var($this->inputs[$name], FILTER_VALIDATE_EMAIL)) {
            $this->errors[$name] = "The '$name' field is not a valid e-mail address.";

            return false;
        }

        return true;
    }

    private function exists($name, $value)
    {
        $value = explode(',', $value);
        $table = $value[0];
        $col = $value[1];

        $sql = (new SQL('select', $table))->addConstraint($col, $this->inputs[$name]);
        $stmt = (new Connector)->connection()->prepare($sql->get());
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $this->errors[$name] = "The value for '$name' does not exist in our database.";
            $stmt = null;

            return false;
        }

        $stmt = null;

        return true;
    }

    private function in($name, $value)
    {
        $values = explode(',', $value);

        if (!in_array($this->inputs[$name], $values)) {
            $this->errors[$name] = "Unexpected value for '$name'.";

            return false;
        }

        return true;
    }

    private function complex($name)
    {
        $uppercase = preg_match('@[A-Z]@', $this->inputs[$name]);
        $lowercase = preg_match('@[a-z]@', $this->inputs[$name]);
        $number = preg_match('@[0-9]@', $this->inputs[$name]);

        if (!$uppercase || !$lowercase || !$number) {
            $this->errors[$name] =  "The value for '$name' should contain at least 1 uppercase character, 1 lowercase character, 1 number.";

            return false;
        }

        return true;
    }
}
