<?php

namespace Internals;

use Internals\View;
use Internals\Session;

class Controller
{
    protected function view($filename, $data = [])
    {
        if (!file_exists(DIR_VIEWS . "/$filename.php")) {
            throw new \Exception("View file '$filename.php' does not exist in " . DIR_VIEWS);
        }

        $data = array_merge($data, [
            'success' => flash('success', null),
            'errors' => flash('errors', null),
        ]);

        echo View::render(DIR_VIEWS . "/$filename.php", $data);

        die;
    }

    protected function with($key, $value)
    {
        Session::flash($key, $value);

        return $this;
    }

    protected function redirect($uri, $permanent = true)
    {
        header("Location: $uri", true, $permanent ? 301 : 302);

        die;
    }
}
