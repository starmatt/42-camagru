<?php

use Internals\Session;
use App\Models\User;

if (!function_exists('str_random')) {

    function str_random($length = 16)
    {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;
            $bytes = random_bytes($size);
            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }
}

if (!function_exists('auth')) {

    function auth()
    {
        return Session::has('auth_id');
    }
}

if (!function_exists('guest')) {

    function guest()
    {
        return !auth();
    }
}

if (!function_exists('session')) {

    function session($key, $default = '')
    {
        return Session::get($key) ?: $default;
    }
}

if (!function_exists('auth_user')) {

    function auth_user()
    {
        return User::current();
    }
}

if (!function_exists('flash')) {

    function flash($key, $default = '')
    {
        return Session::has($key) ? Session::flash($key) : $default;
    }
}

if (!function_exists('back')) {

    function back()
    {
        return array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '/';
    }
}

if (!function_exists('dump')) {

    function dump($val, $printr = false, $backtrace = false)
    {
        echo '<pre>';

        if ($backtrace) {
            debug_print_backtrace();
        }

        $printr && is_array($val) ? print_r($val) : var_dump($val);

        echo '</pre>';
    }
}

if (!function_exists('dd')) {

    function dd($val, $printr = false, $backtrace = false)
    {
        dump($val, $printr, $backtrace);
        die;
    }
}

if (!function_exists('array_keys_exist')) {

    function array_keys_exist(array $keys, array $arr)
    {
        return !array_diff_key(array_flip($keys), $arr);
    }
}

if (!function_exists('imagecreatefromfile')) {

    function imagecreatefromfile($path, $ext)
    {
        switch($ext) {
            case 'bmp':
                return imagecreatefromwbmp($path);
            case 'gif':
                return imagecreatefromgif($path);
            case 'jpeg':
            case 'jpg':
                return imagecreatefromjpeg($path);
            case 'png':
                return imagecreatefrompng($path);
            default:
        }

        return false;
    }
}

if (!function_exists('__')) {

    function __($string)
    {
        return htmlspecialchars($string);
    }
}
