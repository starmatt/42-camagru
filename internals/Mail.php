<?php

namespace Internals;

class Mail
{
    private static $headers = [
        'MIME-Version' => '1.0',
        'Content-type' => 'text/html;charset=UTF-8',
        'From' => 'mborde@student.42.fr',
    ];

    public static function send($to, $subject, $view, $data = [])
    {
        $check = mail($to, $subject, self::getContent($view, $data), self::$headers);

        return $check;
    }

    private static function getContent($view, $data)
    {
        ob_start();

        extract($data);
        require_once DIR_VIEWS . "/emails/$view.php";

        return ob_get_clean();
    }
}
