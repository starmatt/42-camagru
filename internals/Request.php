<?php

namespace Internals;

use App\Models\User;
use Internals\Session;
use Internals\FileInput as File;

class Request
{
    private $inputs = [];

    public function __construct()
    {
        if (auth() && !auth_user()) {
            Session::reset();
        }

        foreach ($_SERVER as $key => $value) {
            $this->{strtolower($key)} = $value;
        }

        if ($this->request_method === 'POST' && isset($_POST)) {
            if (!empty($_POST)) {
                foreach ($_POST as $key => $value) {
                    $this->inputs[strtolower($key)] = trim($value);
                }
            }

            if (!empty($_FILES)) {
                foreach ($_FILES as $key => $value) {
                    $this->inputs[strtolower($key)] = new File($value);
                }
            }
        }

        if ($this->request_method === 'GET' && isset($_GET) && !empty($_GET)) {
            foreach ($_GET as $key => $value) {
                $this->inputs[strtolower($key)] = trim($value);
            }
        }
    }

    public function inputs()
    {
        return $this->inputs;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->inputs);
    }

    public function input($key)
    {
        return $this->has($key) ? $this->inputs[$key] : null;
    }
}
