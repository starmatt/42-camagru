<?php

define('DIR_ROOT', __DIR__);
define('DIR_APP', __DIR__ . '/app');
define('DIR_CONTROLLER', DIR_APP . '/controllers');
define('DIR_MODELS', DIR_APP . '/models');
define('DIR_VIEWS', DIR_APP . '/views');
define('DIR_CONF', __DIR__ . '/config');
define('DIR_STORAGE', __DIR__ . '/storage');
define('DIR_PUBLIC', __DIR__ . '/public');

ini_set('display_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function ($class) {
    $class = explode('\\', $class);

    for ($i = 0; $i + 1 < count($class); $i++) {
        $class[$i] = strtolower($class[$i]);
    }

    require_once DIR_ROOT . '/' . implode('/', $class) . '.php';
});

foreach (glob(DIR_ROOT . '/internals/helpers/*.php') as $file) {
    require_once $file;
}
